package ru.ilinovsg.tm.observer;

import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;
import ru.ilinovsg.tm.service.TaskService;

import static ru.ilinovsg.tm.constant.TerminalConst.*;

public class ListernerTaskImpl implements Listener{
    @Override
    public int update(String command) throws TaskNotFoundException, ProjectNotFoundException {
        TaskService taskService = TaskService.getInstance();

        switch (command) {
            case TASK_CREATE:
                return taskService.createTask();
            case TASK_REMOVE_BY_NAME:
                return taskService.removeTaskByName();
            case TASK_REMOVE_BY_ID:
                return taskService.removeTaskById();
            case TASK_REMOVE_BY_INDEX:
                return taskService.removeTaskByIndex();
            case TASK_UPDATE_BY_NAME:
                return taskService.updateTaskByName();
            case TASK_UPDATE_BY_ID:
                return taskService.updateTaskById();
            case TASK_UPDATE_BY_INDEX:
                return taskService.updateTaskByIndex();
            case TASK_CLEAR:
                return taskService.clearTask();
            case TASK_LIST:
                return taskService.listTask();
            case TASK_VIEW_BY_ID:
                return taskService.viewTaskById();
            case TASK_VIEW_BY_INDEX:
                return taskService.viewTaskByIndex();
            case TASK_VIEW_BY_NAME:
                return taskService.viewTaskByName();
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return taskService.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return taskService.removeTaskFromProjectByIds();
            case TASK_LIST_BY_PROJECT_ID:
                return taskService.listTaskByProjectId();
            case TASK_REMOVE_WITH_PROJECT_BY_ID:
                return taskService.removeTasksAndProject();
            case TASK_LIST_BY_USER_ID:
                return taskService.listTaskByUserId();
            case TASK_ADD_TO_USER_BY_IDS:
                return taskService.addTaskToUserByIds();
            case TASK_REMOVE_FROM_USER_BY_IDS:
                return taskService.removeTaskFromUserByIds();
        }
        return -1;
    }
}
