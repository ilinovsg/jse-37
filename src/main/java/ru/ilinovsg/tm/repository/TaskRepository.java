package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskRepository extends AbstractRepository<Task>{

    private TaskRepository() {
    }

    private static TaskRepository instance = null;

    public static TaskRepository getInstance(){
        synchronized (TaskRepository.class) {
            if (instance == null) {
                instance = new TaskRepository();
            }
        }
        return instance;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Optional<Task> findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return Optional.of(task);
        }
        return Optional.empty();
    }

    public Optional<Task> removeByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.remove(task);
        }
        return Optional.empty();
    }
}
